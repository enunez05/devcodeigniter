<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saldos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Saldos_model');
    }

    public function index()
    {
        $clienteSaldos = $this->Saldos_model->get_cliente_saldos();
        $data['Cliente_saldos'] = $clienteSaldos;

        //Variables de textos
        $data['title_section'] = "Saldos";
        $data['title_subsection'] = "Registro de saldo actual de clientes";
        $data['description'] = "La Siguiente tabla muestra el nombre, apellido y saldo actual del cliente.";

        $data['head'] = "layout/head";
        $data['sidebar_menu'] = "layout/sidebar-menu";
        $data['top_menu'] = "layout/top-menu";
        $data['footer'] = "layout/footer";
        $data['scripts_footer'] = "layout/scripts-footer";

        $data['contenido'] = "content/saldos";
        $this->load->view("master_page_tables",$data);
    }
}