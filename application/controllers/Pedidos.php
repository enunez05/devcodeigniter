<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pedidos_model');
    }

    public function index()
    {
        $reservations = $this->Pedidos_model->get_reservations();
        $data['reservations'] = $reservations;

        //Variables de textos
        $data['title_section'] = "Reservaciones BestWay Cabo";
        $data['title_subsection'] = "Registro de Reservaciones actual de clientes";
        $data['description'] = "La siguiente tabla muestra el registro de las reservaciones actuales de los clientes.";

        $data['head'] = "layout/head";
        $data['sidebar_menu'] = "layout/sidebar-menu";
        $data['top_menu'] = "layout/top-menu";
        $data['footer'] = "layout/footer";
        $data['scripts_footer'] = "layout/scripts-footer";

        $data['contenido'] = "content/pedidos";
        $this->load->view("master_page_tables",$data);


    }
}