<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Persona');
    }
    public function index()
    {
        $personas = $this->Persona->get_personas();
        $data['personas'] = $personas;

        //Variables de textos
        $data['title_section'] = "Clientes";
        $data['title_subsection'] = "Registro de actual de Clientes";
        $data['description'] = "La siguiente tabla muestra el registro total de clientes.";

        $data['head'] = "layout/head";
        $data['sidebar_menu'] = "layout/sidebar-menu";
        $data['top_menu'] = "layout/top-menu";
        $data['footer'] = "layout/footer";
        $data['scripts_footer'] = "layout/scripts-footer";

        $data['contenido'] = "content/clientes";
        $this->load->view("master_page_tables",$data);
    }
    public function user()
    {
        $users_id = '';
        $users = $this->Persona->get_users_details($users_id);
        $data['users'] = $users;
        $this->load->view('person', $data );
    }
}