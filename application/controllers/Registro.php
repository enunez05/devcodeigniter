<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro extends CI_Controller
{

    public function index()
    {
        //Variables de textos
        $data['title_section'] = "Registro de Clientes";
        $data['title_subsection'] = "Formulario de Registro de actual de Clientes";
        $data['description'] = "Llene el siguiente formulario para el registro de usuarios.";


        $data['head'] = "layout/head";
        $data['header'] = "layout/header";
        $data['top_menu'] = "layout/top-menu";
        $data['sidebar_menu'] = "layout/sidebar-menu";
        $data['footer'] = "layout/footer";
        $data['scripts_footer'] = "layout/scripts-footer";

        $data['contenido'] = "content/forms/form_registro";
        $this->load->view("master_form_validation",$data);
    }

}