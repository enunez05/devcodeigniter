<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Pedidos_model extends CI_Model
{

    function get_pedidos()
    {
        $query = $this->db->query('SELECT persona.`Nombres`, persona.`Apellidos`, persona.`Telefono`, persona.`Email`,persona.`Direccion`, persona.`Ciudad`, cliente.`SaldoActual`, pedido.`ImportePedido`,
        pedido.`id_Pedido`,
        producto.`CodigoProducto`,
        producto.`NombreProducto`,
        producto.`DescripcionProducto`,
        producto.`PrecioPublico`
        FROM persona
        INNER JOIN cliente
        ON persona.`id_Persona` = cliente.`idPersona`
        left JOIN pedido
        ON cliente.`id_Cliente` = pedido.`idCliente`
        right JOIN producto
        ON pedido.`id_Pedido` = producto.`idPedido`;');
        return $query;
    }

    function get_reservations()
    {
            $query = $this->db->query('SELECT * FROM reservations ORDER BY id ASC');
            return $query;
    }

}