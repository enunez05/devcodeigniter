<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saldos_model extends CI_Model
{
    function get_cliente_saldos()
    {
        $query = $this->db->query('SELECT cliente.id_Cliente, persona.Nombres,persona.Apellidos, cliente.SaldoActual FROM persona JOIN cliente ON cliente.`idPersona` = persona.`id_Persona`');
        return $query;
    }
}