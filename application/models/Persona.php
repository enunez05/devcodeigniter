<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Persona extends CI_Model
{

    function get_personas()
    {
        return $this->db->get('Persona');
    }

    function get_users_details($user_id)
    {
        $where['id_Persona'] = $user_id;
        return $this->db->get_where('Persona',$where);
    }

}