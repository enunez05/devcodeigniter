<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DOCUMENT</title>
</head>
<body>
<h1>Clientes - Saldos Actuales</h1>
<table>
    <tr>
        <td>NOMBRE</td>
        <td>APELLIDOS</td>
        <td>SALDO ACTUAL</td>
    </tr>
    <?php foreach($Cliente_saldos->result_array() as $row ) { ?>
        <tr>
            <td><?php echo $row['Nombres']; ?></td>
            <td><?php echo $row['Apellidos']; ?></td>
            <td>$<?php echo $row['SaldoActual']; ?></td>
        </tr>
    <?php  } ?>
</table>
<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</body>
</html>
