<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- footer content -->
                <footer>
                    <div class="pull-right">
                        Jenson Design & Developer <a href="https://colorlib.com">Admin Gloria</a>
                        <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>
