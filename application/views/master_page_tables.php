<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
    <?php
        $this->load->view($head);
    ?>
    <!-- Datatables -->
    <link href="<?php echo base_url("vendors") ?>/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url("vendors") ?>/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url("vendors") ?>/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url("vendors") ?>/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url("vendors") ?>/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <?php
                        $this->load->view($sidebar_menu);
                    ?>
                </div>
            </div>
            <!-- top navigation -->
            <?php
                $this->load->view($top_menu);
            ?>
            <!-- /top navigation -->
            <!-- /////////// INICIA CONTENIDO PAGES -->
            <?php
                $this->load->view($contenido);
            ?>
            <!-- ////////// FANALIZA CONTENIDO PAGES -->
            <!-- footer content -->
            <?php
                $this->load->view($footer);
            ?>
            <!-- /footer content -->
        </div>
    </div>
<!-- jQuery -->
<script src="<?php echo base_url("vendors") ?>/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url("vendors") ?>/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url("vendors") ?>/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url("vendors") ?>/nprogress/nprogress.js"></script>

<!-- Datatables -->
<script src="<?php echo base_url("vendors") ?>/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url("vendors") ?>/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url("vendors") ?>/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url("vendors") ?>/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url("vendors") ?>/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url("vendors") ?>/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url("vendors") ?>/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url("vendors") ?>/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url("vendors") ?>/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url("vendors") ?>/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url("vendors") ?>/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url("vendors") ?>/datatables.net-scroller/js/datatables.scroller.min.js"></script>
<script src="<?php echo base_url("vendors") ?>/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url("vendors") ?>/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url("vendors") ?>/pdfmake/build/vfs_fonts.js"></script>

<!-- Custom Theme Scripts -->
<script src="<?php echo base_url("assets/js") ?>/custom.min.js"></script>

<!-- Datatables -->
<script>
    $(document).ready(function() {
        var handleDataTableButtons = function() {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true
                });
            }
        };

        TableManageButtons = function() {
            "use strict";
            return {
                init: function() {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        TableManageButtons.init();
    });
</script>

<!-- /Datatables -->
</body>
</html>