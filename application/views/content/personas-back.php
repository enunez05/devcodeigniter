<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DOCUMENT</title>
</head>
<body>
<table>
    <tr>
        <td>ID</td>
        <td>NOMBRES</td>
        <td>APELLIDOS</td>
        <td>TELÉFONO</td>
        <td>CELULAR</td>
        <td>EMAIL</td>
        <td>DIRECCIÓN</td>
        <td>CIUDAD</td>
    </tr>
    <?php foreach ($users->result() as $row ) { ?>
    <tr>
        <td><?php echo $row->id_Persona; ?></td>
        <td><?php echo $row->Nombres; ?></td>
        <td><?php echo $row->Apellidos; ?></td>
        <td><?php echo $row->Telefono; ?></td>
        <td><?php echo $row->Celular; ?></td>
        <td><?php echo $row->Email; ?></td>
        <td><?php echo $row->Direccion; ?></td>
        <td><?php echo $row->Ciudad; ?></td>
    </tr>
    <?php  } ?>
</table>
<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</body>
</html>
